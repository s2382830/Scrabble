# Scrabble

Authors Aleksandar Nikolov && Andrei Cohan

There are two versions of the game. 
One is a simple single threaded game located in the application folder.
The other is the networked client/server game following the MVC architecture located in the com folder.

To run the simple version:
Run GameTUI from the application folder.
Input name for player1
Input name for player2
Board is printed and a player is chosen at random to being the game.
The tiles of the current player are printed below the board.
There are 3 possible commands to use.
Place, Skip, Exit
Method of use place:
Place word (position of first tile) direction.
Example : Place train H8 H 
Direction  is H for horizontal and V for vertical

To run client/server version:
The server and client both connect through local host
This can be changed by changing the string local host into some other ip
First run the server.java in folder com.server
Once it says server has started on port 8888
Run run two instances of the client.java from the folder com.client
Should see a message on the server each time a new client connects
Use ANNOUNCE playerName on each client to announce yourself to the server
The player name should be distinct as we were unable to provide an error for username already taken, it will assign the players with the same name and then it will be cofusing when trying to play the game as it will say it is the turn of the player but it will say the same name for both

Once both players are connected the game can be played by following the instructions provided in the protocol

P.S. for some reason, there is a bug that I cannot fix where the commands must be inputed multiple times. I dont know why this is but on one client you must input the ANNOUNCE 
command until you get a response saying WELCOME player name

This goes for the other commands aswell and they each 
The game should work as long as the inputs are correct, many of the errors might not work correctly
